import * as loginObj from '../../pageobject/login'

before(() => {
    cy.viewport(1360,980)
})

it('should login', () => {
    loginObj.login()

    loginObj.businessmanagement()
    loginObj.writter()
    loginObj.out()
    loginObj.siteadmin()
    loginObj.click()
    loginObj.logout()
    loginObj.coloumn()
});