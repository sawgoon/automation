export function login(){
    cy.visit('https://dev-idensil.ekbana.net/system/login')
 //Admin Login

    cy.get(':nth-child(2) > .input-group > .form-control').type('admin')
    cy.get(':nth-child(3) > .input-group > .form-control').type('123456')
    cy.get('.btn').click()
}

export function businessmanagement(){
    cy.get(':nth-child(11) > a > .span-link').click()
    cy.get('#addNew').click()

    cy.get('#name').type('Club')
    cy.get('#select2-address-container').type('44000')
    cy.get('.select2-results__option--highlighted').click()
    cy.get(':nth-child(4) > .col-sm-6 > #address').type('ktm')
    cy.get('#reg_id').type('FC250')
    cy.get('#inquiry_fee').type('1000')
    cy.get('#idensil_charge').type('300')

    cy.get(':nth-child(11) > .col-sm-6 > .toggle > .toggle-group > .btn-danger').click()
    cy.get(':nth-child(12) > .col-sm-6 > .toggle > .toggle-group > .btn-danger').click()

    //Create Business User
    cy.get('#names').type('testing')
    cy.get('#username').type('test')
    cy.get('#furigana').type('test')
    cy.get('#email').type('automation@mailinator.com')
    cy.get('#phone_number').type('11111111111')
    cy.get('.offset-sm-2 > .btn-primary').click()
    cy.get('.btn-secondary').click()

}

export function writter(){
    cy.get(':nth-child(19) > a > .span-link').click()
    cy.get('#addNew').click()
    //Create Writer
    cy.get('#name').type('write')
    cy.get('#username').type('write')
    cy.get('#email').type('write@mailinator.com')
    cy.get('#phone_number').type('97864512301')
    cy.get('#address').type('ktm')
    cy.get('.btn-primary').click()

}

export function out(){
    cy.get('.header-avatar').click()
    cy.get('[href="https://dev-idensil.ekbana.net/system/logout"]').click()
}

export function siteadmin(){
    cy.get(':nth-child(2) > .input-group > .form-control').type('siteadmin')
    cy.get(':nth-child(3) > .input-group > .form-control').type('123456')
    cy.get('.btn').click()
}
export function click(){
    cy.get(':nth-child(4) > a > .span-link').click()
    cy.get(':nth-child(5) > a > .span-link').click()
    cy.get(':nth-child(6) > a > .span-link').click()
    cy.get(':nth-child(7) > a > .span-link').click()
    cy.get(':nth-child(14) > a > .span-link').click()

}

export function logout(){
    cy.get('#userDropDown').click()
    cy.get('[href="https://dev-idensil.ekbana.net/system/logout"]').click()


}

export function coloumn(){
    cy.get(':nth-child(2) > .input-group > .form-control').type('ekwriter')
    cy.get(':nth-child(3) > .input-group > .form-control').type('123456')
    cy.get('.btn').click()

    //Create Column
    cy.get(':nth-child(2) > a > .span-link').click()
    cy.get('#addNew').click()
    cy.get('#title').type('New Column')
    cy.get('.select2-search__field').click()
    
    //drop downbox
    cy.get('.select2-search__field')
    .click('Health Services', {force: true})
    .invoke('val')
    .should('eq','Health Services')
    cy.get('#select2-id_label_multiple-result-ipaw-19').click()
    cy.get('#texteditor_ifr').type('Exercise can help prevent excess weight gain or help maintain weight loss.')
    cy.get('.toggle-group > .btn-danger').click()
    cy.get('#services_id').click()
    cy.get(':nth-child(9) > .col-sm-6').click()
    cy.get('#seo_title').type('fit')
    cy.get('#seo_description').type('fit detail')
    cy.get('.btn-primary').click()









}